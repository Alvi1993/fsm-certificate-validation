import { FC, useEffect, useState } from 'react'
import { useRouter } from 'next/router';
import styles from './styles.module.css';
import { getQRcodeImage } from '../certificate';

export const Validate: FC<any> = () => {
  const [queryParams, setQuery] = useState<any>({})
  const [qr, setQr] = useState<any>()

  const { push, query } = useRouter()

  const validateCertification = (e: any) => {
    e.preventDefault()
    let queryString = ''
    if(!queryParams.rut || queryParams.rut.length < 6) {
      alert("Por favor ingresa al menos 6 digitos del rut")
      return
    }
    if(queryParams.rut) {
      queryString += `rut=${queryParams.rut}&`
    }
    if(queryParams.name) {
      queryString += `name=${queryParams.name}&`
    }
    if(queryParams.lastname) {
      queryString += `lastname=${queryParams.lastname}&`
    }
    push(`/certificate?${queryString}`)
  }

  useEffect(() => {
    if(query.rut || query.name || query.lastname) {
      push('/certificate', {
        query
      })
    }
  }, [query])

  useEffect(() => {
    getQRcodeImage("https://certificate.fsmspa.com/validate").then(res => {
      setQr(res)
    })
  }, [])

    return (
      <main className={styles.container}>
          <img className={styles.logo} src="/logo.png" alt="" />

        <div style={{display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center'}}>
          {qr && <img  src={qr} alt="" />}
          <h1>VALIDACIÓN DE CERTIFICADOS</h1>
          <h4>Ingrese a la base de FSM Capacitación y compruebe la información de los documentos emitidos</h4>
        </div>
        <form className={styles.form} onSubmit={validateCertification}>
          <label>A continuación, digíte el RUT del trabajador:</label>
          <input minLength={5} onChange={(e) => setQuery((prev: any) => ({...prev, rut: e.target.value}))} placeholder='Rut - Ejemplo: 12345678-9' />
          <button type='submit'>Validar</button>
        </form>
      </main>
    )
}



export default Validate