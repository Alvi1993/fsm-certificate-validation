import { FC, useEffect, useState } from "react"
import styles from './styles.module.css';
import {useRouter} from "next/router";
import Image from 'next/image'
import Link from "next/link";


export const Politics = () => {

    const [burguerOpen, setBurguerOpen] = useState(false)
    const router = useRouter()

    return (
        <>
            <header className={styles.header}>
        <div className={styles.container_rigth}>
              <div className={styles.container_left}>
                <Link href='/'>
                <Image
                  src={'/logo.png'}
                  alt={'logo'}
                  width={180}
                  height={60}
                  className={styles.logo}
                /></Link>
              
              </div>
              <div className={styles.container_burguer}>
                <button type='button' className={styles.btnBurguer}>
                  <img src={'/web.svg'}  alt={'web'}/>
                </button>
                <button className={styles.btnBurguer} type="button" onClick={(e) => burguerOpen ? setBurguerOpen(false) : setBurguerOpen(true)}>
                  <img src={'/burguer.svg'}  alt={'hamburguer'}/>
              </button>
              </div>
              
              {burguerOpen && <div className={styles.hamburguer}> 
                <span>Certificados</span>
                <span><Link href="/">Validación</Link></span>
                <span><Link href="/politics">Políticas de Privacidad</Link></span>
                <span>Soporte</span>
                <span>Contáctanos</span>
              </div>}
            </div>
      </header>

            <main className={styles.mainPolitics}>
                <h3>POLÍTICA DE PRIVACIDAD</h3>
                <p>La presente Política de Privacidad establece los términos en que FSM SPA usa y protege la información que es proporcionada por sus usuarios al momento de utilizar su app. Esta compañía está comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos llenar los campos de información personal con la cual usted pueda ser identificado, lo hacemos asegurando que sólo se emplea de acuerdo con los términos de este documento. Sin embargo, esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y enfatizamos revisar continuamente esta página para asegurarse que está de acuerdo con dichos cambios. Información que es recogida Nuestra app podrá recoger información personal, por ejemplo: RUT. Así mismo cuando sea necesario podrá ser requerida información específica para procesar algún cambio. Uso de la información recogida Nuestra app emplea la información con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios y mejorar nuestros productos y servicios. FSM SPA está altamente comprometido para cumplir con el compromiso de mantener su información segura. Usamos los sistemas más avanzados y los actualizamos constantemente para asegurarnos que no exista ningún acceso no autorizado. Control de información personal Esta compañía no venderá, cederá ni distribuirá la información personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con una orden judicial. FSM SPA Se reserva el derecho de cambiar los términos de la presente Política de Privacidad en cualquier momento. </p>
            </main>
            <footer className={styles.footerPolitics}>
                <button className={styles.windowBack} type='button' onClick={() => router.back()}>
                  <img src='/arrow-left.svg' alt='flecha'/>
                </button>
            </footer>
            <div className={styles.containerRec}>
              <span className={styles.rectangule1}></span>
              <span className={styles.rectangule2}></span>
              <span className={styles.rectangule3}></span>
            </div>
        </>
    )
}


export default Politics