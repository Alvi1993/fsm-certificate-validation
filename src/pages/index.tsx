import Image from 'next/image'
import { useRouter } from 'next/router'
import styles from './styles/globals.module.css'
import { getQRcodeImage } from './certificate';
import React, { useRef, useState, useEffect } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/effect-fade";


// import required modules
import { Autoplay, Pagination, EffectFade } from "swiper";
import Link from 'next/link';


export const Index = () => {
  /* const router = useRouter()
  useEffect(() => {
    router.push('/validate')
  }, []) */
  const [queryParams, setQuery] = useState<any>({})
  const { push, query } = useRouter()
  const [qr, setQr] = useState<any>()

  useEffect(() => {
    getQRcodeImage("https://certificate.fsmspa.com").then(res => {
      setQr(res)
    })
  }, [])


  useEffect(() => {
    if(query.rut || query.name || query.lastname) {
      push('/certificate', {
        query
      })
    }
  }, [query])


  const validateRut = (e: any) => {
    e.preventDefault()
    let queryString = ''
    if(!queryParams.rut || queryParams.rut.length < 6) {
      alert("Por favor ingresa al menos 6 digitos del rut")
      return
    }
    if(queryParams.rut) {
      queryString += `rut=${queryParams.rut}&`
      sessionStorage.setItem('rut', queryParams.rut)
    }
    if(queryParams.name) {
      queryString += `name=${queryParams.name}&`
    }
    if(queryParams.lastname) {
      queryString += `lastname=${queryParams.lastname}&`
    }
    push(`/certificate?${queryString}`)
  }

  const [donePolitics, setDonePolitics] = useState(false)

  return (
    
    <div className={styles.containerApp}>
      {      
        !donePolitics && <div className={styles.popUp}>
        <div className={styles.popWrap}>
          <div className={styles.textPop}>
            <h3 className={styles.titlePop}>POLÍTICA DE PRIVACIDAD</h3>
            <p className={styles.paragraphPop}>La presente Política de Privacidad establece los términos en que FSM SPA usa y protege la información que es proporcionada por sus usuarios al momento de utilizar su app. Esta compañía está comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos llenar los campos de información personal con la cual usted pueda ser identificado, lo hacemos asegurando que sólo se emplea de acuerdo con los términos de este documento. Sin embargo, esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y enfatizamos revisar continuamente esta página para asegurarse que está de acuerdo con dichos cambios. Información que es recogida Nuestra app podrá recoger información personal, por ejemplo: RUT. Así mismo cuando sea necesario podrá ser requerida información específica para procesar algún cambio. Uso de la información recogida Nuestra app emplea la información con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios y mejorar nuestros productos y servicios. FSM SPA está altamente comprometido para cumplir con el compromiso de mantener su información segura. Usamos los sistemas más avanzados y los actualizamos constantemente para asegurarnos que no exista ningún acceso no autorizado. Control de información personal Esta compañía no venderá, cederá ni distribuirá la información personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con una orden judicial. FSM SPA Se reserva el derecho de cambiar los términos de la presente Política de Privacidad en cualquier momento.</p>
          </div>
          <div className={styles.btnPop}>
            <button type='submit' id='denega' className={styles.btnDenegar}>
              RECHAZAR
            </button>
            <button type="submit" id='acepta' className={styles.btnAceptar} onClick={e => setDonePolitics(true)}>
              ACEPTAR
            </button>
          </div>
        </div>
        </div>
      }
      
    <div className={styles.divideScreen}>

      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        effect={'fade'}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        modules={[Autoplay, Pagination, EffectFade]}
        className="mySwiper"
      >
        <SwiperSlide>
          <Image 
            src={'/slider1.svg'}
            alt={'slider 1'}
            width={200}
            height={200}
            style={{width: '100%', height: '100%', objectFit: 'cover'}}
          />
        </SwiperSlide>
        <SwiperSlide>
        <Image 
            src={'/slider2.svg'}
            alt={'slider 1'}
            width={140}
            height={100}
            style={{width: '100%', height: '100%', objectFit: 'cover'}}
          />
        </SwiperSlide>
        <SwiperSlide>
        <Image 
            src={'/slider3.svg'}
            alt={'slider 1'}
            width={140}
            height={100}
            style={{width: '100%', height: '100%', objectFit: 'cover'}}
          />
        </SwiperSlide>
        <SwiperSlide>
        <Image 
            src={'/slider4.svg'}
            alt={'slider 4'}
            width={140}
            height={100}
            style={{width: '100%', height: '100%', objectFit: 'cover'}}
          />
        </SwiperSlide>
      </Swiper>

    </div>
    <div className={styles.div_right}>
      <header className={styles.header}>
        <Image 
          src="/logo.png"
          alt="logo FSM"
          width={200}
          height={60}
        />
      </header>
      <main className={styles.main}>
        <div style={{display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center', marginTop: '5%'}} className={styles.main_qr}>
            {qr && <img  src={qr} alt="codigo qr" className={styles.qr}/>}
        </div>
        <h3 className={styles.h3}>VALIDACIÓN DE CERTIFICADOS</h3>
        <p className={styles.p}>
          Ingrese a la base de datos de alumnos FSM Capacitación y compruebe la información de los documentos emitidos.
        </p>
        <form className={styles.form} onSubmit={validateRut}>
            <label className={styles.labelRut}>Ingrese el RUT del trabajador:</label>
            <input className={styles.inputRut} minLength={5} onChange={(e) => setQuery((prev: any) => ({...prev, rut: e.target.value}))} placeholder='RUT: ejemplo 12345678-9' />
            <button className={styles.btnValidate} type='submit'>VALIDAR</button>
        </form>
      </main>
      <footer className={styles.footer}>
        <h4 className={styles.contactTo}>Contáctanos por:</h4>
        <div className={styles.socialMedia}>
          <Link href={'https://www.facebook.com/fsmspachile'} target={'_blank'}>
            <Image 
              src={'/redes1.png'}
              alt='logo fb'
              width={42}
              height={40}
            />
          </Link>
          <Link href={'https://www.instagram.com/fsm_spa/'} target={'_blank'}>
            <Image 
              src={'/redes2.png'}
              alt='logo instagram'
              width={42}
              height={40}
            />
          </Link>
          <Link href={'https://www.linkedin.com/in/fsmspa/'} target={'_blank'}>
            <Image 
              src={'/redes3.png'}
              alt='logo linkedin'
              width={42}
              height={40}
            />
          </Link>
          <Link href={'mailto:capacitacion@fsmspa.cl'} target={'_blank'}>
            <Image 
              src={'/redes4.png'}
              alt='logo gmail'
              width={42}
              height={40}
            />
          </Link>
          <Link href={'https://www.google.cl/maps/place/Av.+Arturo+Prat+Chac%C3%B3n+3052,+Iquique,+Tarapac%C3%A1/@-20.2476057,-70.1395204,18.75z/data=!4m5!3m4!1s0x91521470b0b2cc33:0x5d7514941ec9c767!8m2!3d-20.2470492!4d-70.138937?shorturl=1'} target={'_blank'}>
            <Image 
              src={'/redes5.png'}
              alt='logo google maps'
              width={42}
              height={40}
            />
          </Link>
          <Link href={'https://wa.me/+56988974820'} target={'_blank'}>
            <Image 
              src={'/wsp.svg'}
              alt='logo whatsapp'
              width={42}
              height={40}
            />
          </Link>
          
        </div>
        <div className={styles.containerContact}>
        <p className={styles.contactTo}>Nuestra casa matriz se encuentra ubicada en:</p>
        <p className={styles.contactTo}>Avda. Arturo Prat #3052, Iquique, Chile.</p>
        </div>
      </footer>
    </div>
   
      <div className={styles.containerRec}>
        <span className={styles.rectangule1}></span>
        <span className={styles.rectangule2}></span>
        <span className={styles.rectangule3}></span>
      </div>
    </div>
  )
}

export default Index