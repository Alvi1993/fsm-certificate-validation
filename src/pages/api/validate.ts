// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from "axios";
import type { NextApiRequest, NextApiResponse } from "next";
import xlsx from "xlsx";

type Data = any;

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const response = await axios.get(
    "https://fsm-public-dev.s3.us-east-1.amazonaws.com/matriz.xlsx",
    {
      responseType: "arraybuffer",
    }
  );

  const { name, lastname, rut } = req.query;

  const file = xlsx.read(Buffer.from(response.data));

  let data: any[] = [];

  const sheet = file.SheetNames;
  for (let i = 0; i < sheet.length; i++) {
    const temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]]);
    temp.forEach((res) => {
      data.push(res);
    });
  }

  const found = data.filter((x: any) => {
    const matchName = new RegExp(String(name), "gmi").test(
      x["NOMBRES Y APELLIDOS"]
    );
    const matchLastName = RegExp(String(lastname), "gmi").test(
      x["NOMBRES Y APELLIDOS"]
    );
    const matchRut = RegExp(String(rut), "gmi").test(x["RUT"]);
    return (
      (name && name !== "undefined" && matchName) ||
      (lastname && lastname !== "undefined" && matchLastName) ||
      (rut && rut !== "undefined" && matchRut)
    );
  });

  res.status(200).json(found || []);
}
