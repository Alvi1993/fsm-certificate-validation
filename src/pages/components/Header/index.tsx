import Link from 'next/link'
import styles from './header.module.css'
import Image from 'next/image'
import { useEffect, useState } from 'react'

export const Header = () => {

    const [burguerOpen, setBurguerOpen] = useState(false)
    const [brightness, setBrightness] = useState(1)

    useEffect(() => {
      burguerOpen ? setBrightness(1.8) : setBrightness(1)
    }, [burguerOpen, brightness])

    return (
        <>
             <header className={styles.header}>
        <div className={styles.container_rigth}>
              <div className={styles.container_left}>
              <Link href='/'>
                <Image
                  src={'/logo.png'}
                  alt={'logo'}
                  width={180}
                  height={60}
                  className={styles.logo}
                /></Link>

              </div>
              <div className={styles.container_burguer}>
                <button type='button' className={styles.btnBurguer}>
                <Link href={'/'}><img src={'/web.svg'}  alt={'web'}/></Link>
                </button>
                <button className={styles.btnBurguer}  style={{filter: `brightness(${brightness})`}} type="button" onClick={(e) => burguerOpen ? setBurguerOpen(false) : setBurguerOpen(true)}>
                  <img src={'/burguer.svg'}  alt={'hamburguer'}/>
              </button>
              </div>
              
              {burguerOpen && <div className={styles.hamburguer}> 
                <span><Link href={`/certificate?rut=${sessionStorage.getItem('rut')}`}>Certificados</Link></span>
                <span><Link href="/">Validación</Link></span>
                <span><Link href="/politics">Políticas de Privacidad</Link></span>
                <span><Link href={'https://wa.me/+56988974820'} target={'_blank'}>Soporte</Link></span>
              </div>}
            </div>
      </header>
        </>
    )
}

export default Header