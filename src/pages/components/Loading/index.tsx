import styles from './styles.module.css'

const Loading = () => {
    return (
        <>
            <div className={styles.loadingScreen}>
                    <div className={styles.dot}></div>
                    <div className={styles.dot}></div>
                    <div className={styles.dot}></div>
            </div>
        </>
    ) 
}

export default Loading