import { Pagination } from "@mui/material";
import { FC } from "react";
import styles from './styles.module.css'

interface Props {
  total: number;
  setCurrentPage: (currentPage: number) => void;
}

const PaginationAdmin: FC<Props> = ({ total, setCurrentPage }) => {
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setCurrentPage((value - 1) * 12);
  };

  const pages = Math.ceil(total / 12);

  return (
    <Pagination className={styles.pagination} count={pages} color="primary" onChange={handleChange} />
  )
}

export default PaginationAdmin