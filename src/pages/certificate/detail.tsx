import { PDFDownloadLink } from "@react-pdf/renderer";
import axios from "axios";
import moment from "moment";
import "moment/locale/es";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { FC, useEffect, useState } from "react";
import { getQRcodeImage } from ".";
import Header from "../components/Header";
import Loading from "../components/Loading/index";
import CertificatePDF from "./certificate-pdf";
import styles from "./styles.module.css";

export const CertificateDetail: FC<any> = ({}) => {
  const [qr, setQr] = useState<any>({});
  const [data, setData] = useState<any>({});
  const [stateDate, setStateDate] = useState(false);
  const router = useRouter();
  //Loading
  const [isLoading, setLoading] = useState(true);
  //Progress Bar
  const [newProgress, setNewProgress] = useState(
    `conic-gradient(rgba(0, 24, 129, 1) 3.6deg, #f5f5f5 0deg)`
  );

  const date = moment(new Date(Date.UTC(0, 0, data?.["10/31/25"]))).format(
    "MMMM DD,YYYY"
  );
  useEffect(() => {
    if (date) {
      const validateDate = new Date(
        Date.UTC(0, 0, data?.["10/31/25"])
      ).getTime();
      validateDate > new Date().getTime()
        ? setStateDate(true)
        : setStateDate(false);
    }
  }, [data]);

  useEffect(() => {
    let queryString = "";
    if (router.query.rut) {
      queryString += `rut=${router.query.rut}&`;
    }
    if (router.query.course) {
      queryString += `course=${encodeURIComponent(
        String(router.query.course)
      )}&`;
    }
    findDataCertificateDetail(router.query).then((res) => {
      setData(res);
      setLoading(false);
    });
    getQRcodeImage(
      "https://certificate.fsmspa.com/certificate/detail?" + queryString
    ).then((res: any) => {
      setQr(res);
    });
  }, [router]);

  useEffect(() => {
    if (typeof data["NOTA FINAL"] == "number" && data["NOTA FINAL"] > 1) {
      const validateNote = data["NOTA FINAL"];
      setNewProgress(
        `conic-gradient(rgba(0, 24, 129, 1) ${
          validateNote * 3.6
        }deg, #f5f5f5 0deg`
      );
    }

    if (typeof data["NOTA FINAL"] == "number" && data["NOTA FINAL"] <= 1) {
      const validateNote = data["NOTA FINAL"];
      setNewProgress(
        `conic-gradient(rgba(0, 24, 129, 1) ${
          validateNote * 100 * 3.6
        }deg, #f5f5f5 0deg`
      );
    }

    if (typeof data["NOTA FINAL"] !== "number") {
      const preValidate = data["NOTA FINAL"];
      const validateNote = Number(preValidate?.split("%", 1).join(""));
      setNewProgress(
        `conic-gradient(rgba(0, 24, 129, 1) ${
          validateNote * 3.6
        }deg, #f5f5f5 0deg`
      );
    }
  }, [data]);

  if (isLoading) return <Loading />;
  return (
    <>
      <Header></Header>
      <div className={styles.container}>
        <div className={styles.detailsContainer}>
          <div className={styles.container_name}>
            <h3 className={styles.name}>{data["NOMBRES Y APELLIDOS"]}</h3>
          </div>
          <div className={styles.avatar}>
            <Image src={"/user.png"} alt={"avatar"} width={70} height={70} />
          </div>
          <div className={styles.corp}>
            <h3 className={styles.corp_empresa}>{data["EMPRESA"]}</h3>
            <p className={styles.corp_rut}>{data["RUT"]}</p>
          </div>
        </div>
        <main className={styles.mainCourse}>
          <h3>{data["CURSO/RECERTIFICACIÓN"]}</h3>
          <span className={styles.spanVerificate}>
            {data["NUMERO VERIFICACIÓN"]}
          </span>
          <p>Aprobó Exitosamente con:</p>
          <div className={styles.progressBar}>
            <div
              style={{ background: newProgress }}
              className={styles.progressBar_circle}
            >
              <span className={styles.value}>
                {typeof data["NOTA FINAL"] == "string"
                  ? data["NOTA FINAL"]
                  : data["NOTA FINAL"] > 1
                  ? Math.round(data["NOTA FINAL"]) + "%"
                  : Math.round(data["NOTA FINAL"] * 100) + "%"}{" "}
              </span>
            </div>
          </div>
          <p>Estado de Validación</p>
          {stateDate ? (
            <h2>VIGENTE</h2>
          ) : (
            <h2 className={styles.stateDateVen}>VENCIDO</h2>
          )}
        </main>
        <footer className={styles.footerDetail}>
          <button
            className={styles.windowBack}
            type="button"
            onClick={() => router.back()}
          >
            <img src="/arrow-left.svg" alt="flecha" />
          </button>
          <div className={styles.fechaVen}>
            <h3 className={styles.fechaDetails}>Fecha de vencimiento</h3>
            <p className={styles.fechaDetails}>{String(date)}</p>
          </div>
          <div className={styles.downloadDetails}>
            <PDFDownloadLink
              document={<CertificatePDF data={data} qr={qr} />}
              fileName={`CERTIFICADO-${data["NOMBRES Y APELLIDOS"]}`}
              style={{ textDecoration: "none" }}
            >
              {({ loading }) =>
                loading ? (
                  <span className={styles.downloadDetails}>Cargando...</span>
                ) : (
                  <div
                    style={{
                      cursor: "pointer",
                      display: "flex",
                      flexDirection: "column",
                      gap: "4px",
                    }}
                  >
                    <img
                      src="/download.svg"
                      alt="flecha"
                      style={{ margin: "0 auto" }}
                    />
                    <span className={styles.downloadDetails}>
                      Descargar Certificado en PDF
                    </span>
                  </div>
                )
              }
            </PDFDownloadLink>
          </div>
        </footer>
        <div className={styles.soporte}>
          <p>Soporte</p>
          <Link href={"https://wa.me/+56988974820"} target={"_blank"}>
            <Image
              src={"/wsp.svg"}
              alt="logo whatsapp"
              width={48}
              height={46}
            />
          </Link>
        </div>
        <div className={styles.containerRec}>
          <span className={styles.rectangule1}></span>
          <span className={styles.rectangule2}></span>
          <span className={styles.rectangule3}></span>
        </div>
      </div>
    </>
  );
};

export async function findDataCertificateDetail(query: any) {
  let queryString = "";
  if (query.rut) {
    queryString += `rut=${query.rut}&`;
  }
  if (query.course) {
    queryString += `course=${query.course}&`;
  }
  /*  const res = await axios.get(`https://certificate.fsmspa.com/api/detail?${queryString}`) */
  const res = await axios.get(
    `${
      process.env.NEXT_PUBLIC_HOST || process.env.NEXT_PUBLIC_URL
    }/api/detail?${queryString}`
  );
  const data = res && (await res.data);
  return data;
}
export default CertificateDetail;
