import styles from './styles.module.css'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useState } from 'react'



export const CertificateDone = () => {

    const router = useRouter()
    const [burguerOpen, setBurguerOpen] = useState(false) 
    
    return(
        <>
          <div className={styles.container}>
            <div className={styles.container_rigth}>
              <div className={styles.container_left}>
                <Image
                  src={'/logo.png'}
                  alt={'logo'}
                  width={180}
                  height={60}
                />

              </div>
              <button type='button'>
                <img src={'/web.svg'}  alt={'web'}/>
              </button>
              <button className={styles.btnBurguer} type="button" onClick={(e) => burguerOpen ? setBurguerOpen(false) : setBurguerOpen(true)}>
                <img src={'/burguer.svg'}  alt={'hamburguer'}/>
              </button>
              {burguerOpen && <div className={styles.hamburguer}> 
                <span>Certificados</span>
                <span>Validación</span>
                <span>Políticas de privacidad</span>
                <span>Soporte</span>
                <span>Contáctanos</span>
              </div>}
            </div>
        </div>
        <main className={styles.mainDone}>
            <h3>Su certificado se ha descargado con éxito.</h3>
        </main>
        <footer className={styles.footerDone}>
            <div className={styles.socialMedia}>
                <Image 
                src={'/redes1.png'}
                alt='logo fb'
                width={48}
                height={46}
                />
                <Image 
                src={'/redes2.png'}
                alt='logo instagram'
                width={48}
                height={46}
                />
                <Image 
                src={'/redes3.png'}
                alt='logo linkedin'
                width={48}
                height={46}
                />
                <Image 
                src={'/redes4.png'}
                alt='logo gmail'
                width={48}
                height={46}
                />
            </div>
        </footer>
    </>
    )
}

export default CertificateDone