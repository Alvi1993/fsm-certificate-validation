import {
  Document,
  Image,
  Page,
  StyleSheet,
  Text,
  View,
} from "@react-pdf/renderer";

import { Font } from "@react-pdf/renderer";
import moment from "moment";
Font.register({ family: "Arial", src: "/arial.ttf" });

const styles = StyleSheet.create({
  qr: {
    position: "absolute",
    top: 30,
    left: 20,
    width: "110px",
    height: "110px",
    borderRadius: "6px",
  },
  section: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    justifyContent: "center",
    margin: "2x",
    alignItems: "center",
  },
  sectionName: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: "5px",
    marginBottom: "20px",
  },
  sectionCourse: {
    display: "flex",
    width: "100%",
    textAlign: "center",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    gap: "5px",
    marginBottom: "15px",
  },
  image: {
    position: "absolute",
    width: "100%",
    height: "100%",
    minWidth: "100%",
    minHeight: "100%",
    display: "flex",
  },

  textGray: {
    color: "#94A2B1",
    letterSpacing: 1.5,
    fontWeight: "bold",
    fontSize: "13px",
  },

  textGoldTitle: {
    fontSize: 25,
    color: "#D59429",
    fontStyle: "bold",
  },
  textGoldCourse: {
    width: "60%",
    textAlign: "center",
    alignSelf: "center",

    fontSize: 15,
    color: "#D59429",
  },
  textGold: {
    color: "#D59429",
    marginTop: "7px",
    letterSpacing: 1.5,
  },
  logo: {
    marginTop: "35px",
    marginBotton: "20px",
    width: "30%",
    height: "70px",
  },

  verificateNumber: {
    color: "#364D5D",
    marginTop: "7px",
    fontSize: 9,
    position: "absolute",
    fontFamily: "Arial",
    fontWeight: "bold",
    width: "100%",
    top: 70,
  },

  footer: {
    margin: "0 auto",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    width: "75%",
    /*     marginLeft: "10px", */
    /*     marginTop: "20px", */
  },
});

function CertificatePDF({ data, qr }: any) {
  return (
    <Document>
      <Page size={"LETTER"} orientation="landscape">
        <Image src={"/Fondo2.png"} style={styles.image} />
        <View style={styles.section}>
          <Image src={qr} style={styles.qr} />
          <Text
            style={{
              fontSize: 10,
              position: "absolute",
              top: 142,
              left: 40,
              fontWeight: "bold",
              fontFamily: "Arial",
            }}
          >
            QR Verificador
          </Text>
          <Image src={"/fsm_cap.png"} style={styles.logo} />
          <Text
            style={{
              fontSize: 45,
              width: "60%",
              textAlign: "center",
              fontFamily: "Arial",
              fontWeight: "bold",
              letterSpacing: 1.5,
              marginTop: "35px",
              color: "#324A5A",
              marginBottom: "15px",
            }}
          >
            CERTIFICACIÓN
          </Text>
          <View style={styles.sectionName}>
            <Text style={styles.textGray}>Este certificado es otorgado a:</Text>
            <Text
              style={{
                fontSize: 20,
                textAlign: "center",
                fontFamily: "Arial",
                fontWeight: "bold",
                color: "#D59429",
                marginBottom: "10px",
              }}
            >
              {data?.["NOMBRES Y APELLIDOS"]}
            </Text>
            <Text style={styles.textGray}>
              RUT <Text style={styles.textGold}>{data?.["RUT"]}</Text>
            </Text>
          </View>

          <View style={styles.sectionName}>
            <Text style={styles.textGray}>De la empresa:</Text>
            <Text style={styles.textGold}>{data?.["EMPRESA"]}</Text>
          </View>

          <View style={styles.sectionCourse}>
            <Text style={styles.textGray}>
              Aprobando la siguiente capacitación:
            </Text>
            <Text style={styles.textGoldCourse}>
              {data?.["CURSO/RECERTIFICACIÓN"]}
            </Text>
          </View>

          <View style={{ marginBottom: "55px" }}>
            <Text style={styles.textGray}>
              Con un % final de:{" "}
              <Text style={styles.textGold}>
                {typeof data?.["NOTA FINAL"] == "string"
                  ? data?.["NOTA FINAL"]
                  : data?.["NOTA FINAL"] > 1
                  ? Math.round(data?.["NOTA FINAL"]) + "%"
                  : Math.round(data?.["NOTA FINAL"] * 100) + "%"}
              </Text>
            </Text>
          </View>
        </View>
        <View style={styles.footer}>
          <View style={styles.verificateNumber}>
            <Text>NÚMERO DE VERIFICACIÓN: {data?.["NUMERO VERIFICACIÓN"]}</Text>
          </View>
          <View
            style={{
              width: "30%",
              color: "#364D5D",
              fontFamily: "Arial",
              fontWeight: "bold",
              fontSize: "10px",
            }}
          >
            <Text>
              REALIZADO:{" "}
              {moment(new Date(Date.UTC(0, 0, data?.["FECHA CURSO"]))).format(
                "DD/MM/YYYY"
              )}
            </Text>
            <Text>
              VALIDO HASTA:{" "}
              {moment(new Date(Date.UTC(0, 0, data?.["10/31/25"]))).format(
                "DD/MM/YYYY"
              )}
            </Text>
          </View>
          <View style={{ width: "20%", marginRight: "15px" }}>
            <Image
              src={"/port.png"}
              style={{ width: "100px", height: "100px" }}
            />
          </View>
          <View style={{ width: "20%" }}>
            <Image
              src={"/firma.png"}
              style={{ width: "100px", height: "80px" }}
            />
          </View>
        </View>
      </Page>
    </Document>
  );
}

export default CertificatePDF;
