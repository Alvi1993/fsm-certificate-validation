import styled from "@emotion/styled";

export const DivTitle = styled.div`
  display: flex;
  width: 100%;
  height: 100px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-size: contain;
  background-repeat: no-repeat;
`;

export default DivTitle;
