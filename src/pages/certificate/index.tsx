import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import QRCode from "qrcode";
import { useEffect, useState } from "react";
import Header from "../components/Header/index";
import Loading from "../components/Loading";
import Pagination from "../components/Pagination";
import styles from "./styles.module.css";

export async function getQRcodeImage(
  dataForQRcode: string,
  logo = "https://certificate.fsmspa.com/qr-logo.jpg",
  width = 205,
  cwidth = 60
) {
  return QRCode.toDataURL(dataForQRcode);
}

export async function findDataCertificate(query: any) {
  let queryString = "";
  if (query.rut) {
    queryString += `rut=${query.rut}&`;
  }
  if (query.name) {
    queryString += `name=${query.name}&`;
  }
  if (query.lastname) {
    queryString += `lastname=${query.lastname}&`;
  }
  /* const res = await axios.get(`https://certificate.fsmspa.com/api/validate?${queryString}`) */
  const res = await axios.get(
    `${
      process.env.NEXT_PUBLIC_HOST || process.env.NEXT_PUBLIC_URL
    }/api/validate?${queryString}`
  );
  const data = res && (await res.data);
  return data;
}

export const Certificate = () => {
  const router = useRouter();
  const [data, setData] = useState<any>([]);

  useEffect(() => {
    findDataCertificate(router.query).then((res) => {
      setData(res);
      setLoading(false);
    });
  }, [router]);

  useEffect(() => {
    if (data.length && router.query.course) {
    }
  }, [data, router]);

  const onClick = (data: any) => {
    router.push(
      "/certificate/detail?" +
        `rut=${data["RUT"]}&course=${encodeURIComponent(
          data["CURSO/RECERTIFICACIÓN"]
        )}`
    );
  };

  //Loading
  const [isLoading, setLoading] = useState(true);

  /* === Pagination === */
  const [routeList, setRouteList] = useState<[]>();
  const [currentRouteList, setCurrentRouteList] = useState<[]>();
  const [currentPage, setCurrentPage] = useState<number>(0);
  const [totalPages, setTotalPages] = useState<number>(0);

  useEffect(() => {
    if (data) {
      setRouteList(data);
      setCurrentPage(data.slice(currentPage, 12));
      setTotalPages(data.length);
    }
  }, [data]);

  useEffect(() => {
    if (data) {
      setCurrentRouteList(data.slice(currentPage, currentPage + 12));
    }
  }, [currentPage]);

  if (isLoading) return <Loading />;

  return (
    <>
      <Header></Header>
      <main className={styles.main}>
        <h1>Bienvenido</h1>
        <div className={styles.infoRut}>
          <h3 className={styles.infoRut_name}>
            {data.length > 0 &&
              (data?.[0]?.["NOMBRES Y APELLIDOS"]).toLowerCase()}
          </h3>
          <div className={styles.infoRut_left}>
            <p className={styles.infoRut_left__p}>N° Identidad</p>
            <p className={styles.infoRut_rut}>
              {data.length > 0 && "Rut: " + data?.[0]?.["RUT"]}
            </p>
          </div>
          <div className={styles.infoRut_right}>
            <p className={styles.infoRut_right__p}>EMPRESA</p>
            <h3 className={styles.infoRut_corp}>
              {data.length > 0 && data?.[0]?.["EMPRESA"]}
            </h3>
          </div>
        </div>
      </main>

      <p className={styles.dataView}>Visualice los certificados obtenidos:</p>
      <div className={styles.grid}>
        {data?.map((certificate: any, i: number) => {
          return (
            <div key={i} onClick={() => onClick(certificate)}>
              <h2 className={styles.grid_courseName}>
                {certificate["CURSO/RECERTIFICACIÓN"]}
              </h2>
            </div>
          );
        })}
      </div>
      <Pagination total={totalPages} setCurrentPage={setCurrentPage} />
      <div className={styles.soporteIndex}>
        <p>Soporte</p>
        <Link href={"https://wa.me/+56988974820"} target={"_blank"}>
          <Image src={"/wsp.svg"} alt="logo whatsapp" width={48} height={46} />
        </Link>
      </div>

      <div className={styles.containerRec}>
        <span className={styles.rectangule1}></span>
        <span className={styles.rectangule2}></span>
        <span className={styles.rectangule3}></span>
      </div>
    </>
  );
};

export default Certificate;
